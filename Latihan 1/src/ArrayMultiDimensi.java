public class ArrayMultiDimensi {
    public static void main (String[] args) {
        String Mahasiswa [][]= {
            {
                "021210009", "Anselmus Yoga Pamungkas"
            },
            {
                "021210049", "Shelly Meilinda"
            },
            {   
                "021210067", "Epa Froditus Luohumbowo"
            },
            {
                "021210065", "Alham"
            },
            {
                "021210039", "Riki Ronaldo"
            }
        };
        System.out.println("Nama" + Mahasiswa[0][1] + ""
                + " Berada pada baris 1, baris ke 2");
        System.out.println("NPM" + Mahasiswa[3][0] + ""
                + " Berada pada baris 4, baris ke 1");

    }
}